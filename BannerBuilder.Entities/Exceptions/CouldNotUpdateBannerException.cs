﻿using System;

namespace BannerBuilder.Entities.Exceptions
{
    public class CouldNotUpdateBannerException : Exception
    {
        public CouldNotUpdateBannerException(string message) : base(message) { }
    }
}
