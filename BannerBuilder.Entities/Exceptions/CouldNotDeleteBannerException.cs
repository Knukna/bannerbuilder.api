﻿using System;

namespace BannerBuilder.Entities.Exceptions
{
    public class CouldNotDeleteBannerException : Exception
    {
        public CouldNotDeleteBannerException(string message) : base(message) { }
    }
}
