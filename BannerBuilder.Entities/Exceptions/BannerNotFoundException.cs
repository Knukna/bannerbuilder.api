﻿using System;

namespace BannerBuilder.Entities.Exceptions
{
    public class BannerNotFoundException: Exception
    {
        public BannerNotFoundException(string message) : base(message){}
    }
}
