﻿using System;

namespace BannerBuilder.Entities.Exceptions
{
    public class HtmlInvalidException : Exception
    {
        public HtmlInvalidException(string message): base(message) { }
    }
}
