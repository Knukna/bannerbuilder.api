﻿using System;

namespace BannerBuilder.Entities.DbEntities
{
    public class Banner
    {
        public Guid Id { get; set; }
        public string Html { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }
    }
}
