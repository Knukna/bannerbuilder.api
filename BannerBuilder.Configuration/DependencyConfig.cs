﻿using Autofac;
using BannerBuilder.Contracts.BLL;
using BannerBuilder.Contracts.DAL;
using BannerBuilder.Repositories;
using BannerBuilder.Services;

namespace BannerBuilder.Configurations
{
    public static class DependencyConfig
    {
        public static ContainerBuilder RegisterRepositories(this ContainerBuilder builder)
        {
            builder
                .RegisterType<BannerRepository>()
                .As<IBannerRepository>()
                .SingleInstance();

            return builder;
        }

        public static ContainerBuilder RegisterServices(this ContainerBuilder builder)
        {
            builder
                .RegisterType<BannerBuilderService>()
                .As<IBannerBuilderService>()
                .SingleInstance();

            return builder;
        }
    }
}
