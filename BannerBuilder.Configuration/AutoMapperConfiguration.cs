﻿using AutoMapper;
using BannerBuilder.Entities.DbEntities;
using BannerBuilder.ViewModels.Response;

namespace BannerBuilder.Configurations
{
    public static class AutoMapperConfiguration
    {
        public static void Configure()
        {
            Mapper.Initialize(config => config.CreateMap<Banner, BannerModelResponse>());
        }
    }
}
