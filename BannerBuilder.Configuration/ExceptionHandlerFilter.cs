﻿using System.Net;
using System.Net.Http;
using System.Web.Http.Filters;
using BannerBuilder.Entities.Exceptions;

namespace BannerBuilder.Configurations
{
    //Added this class to handle custom exceptions
    public class ExceptionHandlerFilter : ExceptionFilterAttribute
    {
        public override void OnException(HttpActionExecutedContext context)
        {
            HandleException(context);
        }

        private void HandleException(HttpActionExecutedContext context)
        {
            context.Response = new HttpResponseMessage();

            if (context.Exception is BannerNotFoundException)
            {
                context.Response.StatusCode = HttpStatusCode.NotFound;
                context.Response.Content = new StringContent(context.Exception.Message);
            }
            else if (context.Exception is CouldNotDeleteBannerException ||
                     context.Exception is CouldNotUpdateBannerException)
            {
                context.Response.StatusCode = HttpStatusCode.InternalServerError;
                context.Response.Content = new StringContent(context.Exception.Message);
            }
            else if (context.Exception is HtmlInvalidException)
            {
                context.Response.StatusCode = HttpStatusCode.BadRequest;
                context.Response.Content = new StringContent(context.Exception.Message);
            }
            else
            {
                context.Response.StatusCode = HttpStatusCode.InternalServerError;
                context.Response.Content = new StringContent("Something went wrong, sorry!");
            }
        }
    }
}
