﻿using System;
using System.Threading.Tasks;
using BannerBuilder.Entities.DbEntities;

namespace BannerBuilder.Contracts.DAL
{
    public interface IBannerRepository
    {
        Task<Guid> CreateBanner(string newBannerBody);
        Task<Banner> GetBanner(Guid bannerId);
        Task<bool> DeleteBanner(Guid bannerId);
        Task<bool> UpdateBanner(Banner updatedBanner);
    }
}
