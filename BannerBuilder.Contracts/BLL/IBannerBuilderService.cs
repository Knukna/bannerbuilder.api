﻿using System;
using System.Threading.Tasks;
using BannerBuilder.ViewModels.Requests;
using BannerBuilder.ViewModels.Response;

namespace BannerBuilder.Contracts.BLL
{
    public interface IBannerBuilderService
    {
        Task<Guid> CreateBanner(BannerModelRequest newBanner);
        Task<BannerModelResponse> GetBanner(Guid bannerId);
        Task UpdateBanner(Guid bannerId, BannerModelRequest updatedBanner);
        Task DeleteBanner(Guid bannerId);
    }
}
