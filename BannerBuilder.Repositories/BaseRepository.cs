﻿using System.IO;
using LiteDB;

namespace BannerBuilder.Repositories
{
    public static class BaseRepository
    {
        public static LiteDatabase Db()
        {
            CreateDbDirectoryIfNotExist();
            return new LiteDatabase(@"C:\BannerBuilderDb\BannerBuilder.db");
        }

        private static void CreateDbDirectoryIfNotExist()
        {
            Directory.CreateDirectory(@"C:\BannerBuilderDb");
        }
    }
}
