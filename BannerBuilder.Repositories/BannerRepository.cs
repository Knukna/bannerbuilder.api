﻿using System;
using System.Threading.Tasks;
using BannerBuilder.Contracts.DAL;
using BannerBuilder.Entities.DbEntities;
using LiteDB;

namespace BannerBuilder.Repositories
{
    public class BannerRepository : IBannerRepository
    {

        public async Task<Guid> CreateBanner(string newBannerBody)
        {
            return await Task.Run(() =>
            {
                using (var db = BaseRepository.Db())
                {
                    var bannerCollection = GetBannerCollection(db);
                    var newBanner = GetBannerEntity(newBannerBody);
                    newBanner.Id = bannerCollection.Insert(newBanner);

                    return newBanner.Id;
                }
            });
        }

        private LiteCollection<Banner> GetBannerCollection(LiteDatabase db)
        {
            var bannerCollection = db.GetCollection<Banner>("banners");

            //create index on banner.Id if not exist
            bannerCollection.EnsureIndex(x => x.Id);

            return bannerCollection;
        }

        private Banner GetBannerEntity(string newBannerBody)
        {
            return new Banner
            {
                Created = DateTime.Now,
                Modified = DateTime.Now,
                Html = newBannerBody
            };
        }

        public async Task<Banner> GetBanner(Guid bannerId)
        {
            return await Task.Run(() =>
            {
                using (var db = BaseRepository.Db())
                {
                    var bannerCollection = GetBannerCollection(db);
                    var banner = bannerCollection.FindById(bannerId);
                    return banner;
                }
            });
        }

        public async Task<bool> DeleteBanner(Guid bannerId)
        {
            return await Task.Run(() =>
            {
                using (var db = BaseRepository.Db())
                {
                    var bannerCollection = GetBannerCollection(db);
                    var deleted = bannerCollection.Delete(bannerId);
                    return deleted;
                }
            });
        }

        public async Task<bool> UpdateBanner(Banner updatedBanner)
        {
            return await Task.Run(() =>
            {
                using (var db = BaseRepository.Db())
                {
                    var bannerCollection = GetBannerCollection(db);
                    var updated = bannerCollection.Update(updatedBanner.Id, updatedBanner);
                    return updated;
                }
            });
        }
    }
}
