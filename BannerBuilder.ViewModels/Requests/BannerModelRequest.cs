﻿using System.ComponentModel.DataAnnotations;

namespace BannerBuilder.ViewModels.Requests
{
    public class BannerModelRequest
    {
        [Required]
        public string Html { get; set; }

    }
}
