﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BannerBuilder.ViewModels.Response
{
    public class BannerModelResponse
    {
        public Guid Id { get; set; }
        public string Html { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }
    }
}
