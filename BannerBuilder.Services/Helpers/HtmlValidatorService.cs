﻿using System;
using System.Xml;

namespace BannerBuilder.Services.Helpers
{
    public static class HtmlValidatorService
    {
        public static bool IsValidHtml(string html)
        {
            try
            {
                var htmlDocument = new XmlDocument();
                htmlDocument.LoadXml(html);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}
