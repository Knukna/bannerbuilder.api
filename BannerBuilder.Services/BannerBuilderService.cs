﻿using System;
using System.Threading.Tasks;
using AutoMapper;
using BannerBuilder.Contracts.BLL;
using BannerBuilder.Contracts.DAL;
using BannerBuilder.Entities.DbEntities;
using BannerBuilder.Entities.Exceptions;
using BannerBuilder.Services.Helpers;
using BannerBuilder.ViewModels.Requests;
using BannerBuilder.ViewModels.Response;

namespace BannerBuilder.Services
{
    public class BannerBuilderService : IBannerBuilderService
    {
        private readonly IBannerRepository _bannerRepository;
        private const string InvalidHtmlMessage = "Invalid html";
        private const string CouldNotUpdateBannerMessage = "Could not update banner";
        private const string CouldNotDeleteBannerMessage = "Could not delete banner";
        private const string BannerNotFoundMessage = "Could not find banner";

        public BannerBuilderService(IBannerRepository bannerRepository)
        {
            _bannerRepository = bannerRepository;
        }

        public async Task<Guid> CreateBanner(BannerModelRequest request)
        {
            var validHtml = HtmlValidatorService.IsValidHtml(request.Html);
            if(!validHtml) throw new HtmlInvalidException(InvalidHtmlMessage);

            var result = await _bannerRepository.CreateBanner(request.Html);
            return result;
        }

        public async Task<BannerModelResponse> GetBanner(Guid bannerId)
        {
            var banner = await _bannerRepository.GetBanner(bannerId);
            if(banner == null) throw new BannerNotFoundException(BannerNotFoundMessage);

            return MapBannerToBannerResponseModel(banner);
        }

        private static BannerModelResponse MapBannerToBannerResponseModel(Banner banner)
        {
            return Mapper.Map<BannerModelResponse>(banner);
        }

        public async Task DeleteBanner(Guid bannerId)
        {
            var deleted = await _bannerRepository.DeleteBanner(bannerId);
            if(!deleted) throw new CouldNotDeleteBannerException(CouldNotDeleteBannerMessage);
        }

        public async Task UpdateBanner(Guid bannerId, BannerModelRequest request)
        {
            var validHtml = HtmlValidatorService.IsValidHtml(request.Html);
            if (!validHtml) throw new HtmlInvalidException(InvalidHtmlMessage);

            var banner = await _bannerRepository.GetBanner(bannerId);
            if(banner == null) throw new BannerNotFoundException(BannerNotFoundMessage);

            banner = UpdateBannerEntity(banner, request.Html);
          
            var updated = await _bannerRepository.UpdateBanner(banner);
            if(!updated) throw new CouldNotUpdateBannerException(CouldNotUpdateBannerMessage);
        }

        private static Banner UpdateBannerEntity(Banner bannerToUpdate, string html)
        {
            bannerToUpdate.Html = html;
            bannerToUpdate.Modified = DateTime.Now;

            return bannerToUpdate;
        }
    }
}
