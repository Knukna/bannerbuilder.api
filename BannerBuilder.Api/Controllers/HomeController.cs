﻿using System.Web.Mvc;

namespace BannerBuilder.Api.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}