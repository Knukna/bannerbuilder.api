﻿using System;
using System.Threading.Tasks;
using System.Web.Http;
using BannerBuilder.Contracts.BLL;
using BannerBuilder.ViewModels.Requests;

namespace BannerBuilder.Api.Controllers
{
    [RoutePrefix("api/banner")]
    public class BannerBuilderController : ApiController
    {
        private readonly IBannerBuilderService _bannerBuilderService;

        public BannerBuilderController(IBannerBuilderService bannerBuilderService)
        {
            _bannerBuilderService = bannerBuilderService;
        }

        [HttpPost]
        [Route("")]
        public async Task<IHttpActionResult> CreateBanner([FromBody] BannerModelRequest newBanner)
        {
            var result = await _bannerBuilderService.CreateBanner(newBanner);
            return Created("api/banner/" + result, result.ToString());
        }

        [HttpGet]
        [Route("{bannerId}")]
        public async Task<IHttpActionResult> GetBanner([FromUri] Guid bannerId)
        {
            var banner = await _bannerBuilderService.GetBanner(bannerId);
            return Ok(banner);
        }

        [HttpPut]
        [Route("{bannerId}")]
        public async Task<IHttpActionResult> UpdateBanner([FromUri] Guid bannerId, [FromBody] BannerModelRequest updatedBanner)
        {
            await _bannerBuilderService.UpdateBanner(bannerId, updatedBanner);
            return Ok();
        }

        [HttpDelete]
        [Route("{bannerId}")]
        public async Task<IHttpActionResult> DeleteBanner([FromUri] Guid bannerId)
        {
            await _bannerBuilderService.DeleteBanner(bannerId);
            return Ok();
        }
    }
}
