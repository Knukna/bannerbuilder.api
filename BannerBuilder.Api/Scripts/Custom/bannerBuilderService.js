﻿function GetBanner() {
    $.ajax({
        type: "GET",
        url: "/api/banner/" + $("#getBanner").val(),
        dataType: "json",
        async: true,
        success: function (data) {
            $("#banner").html(data["Html"]);
            $("#error").html("");

        },
        error: function (xhr) {
            $("#banner").html("");
            $("#error").html("<h3>Något gick fel! Felmeddelande:</h3><pre> " + xhr.responseText + " (" + xhr.status + ")</pre>");
        }
    });
}