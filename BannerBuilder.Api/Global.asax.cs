﻿using System;
using System.Web;
using System.Web.Http;
using System.Web.Routing;
using BannerBuilder.Configurations;

namespace BannerBuilder.Api
{
    public class Global : HttpApplication
    {
        void Application_Start(object sender, EventArgs e)
        {
            // Code that runs on application startup
            
            //Register web api config
            WebApiConfig.Register(GlobalConfiguration.Configuration);
            
            //Register web routes 
            RouteConfig.RegisterRoutes(RouteTable.Routes);

            //Configure dependencies
            GlobalConfiguration.Configure(DependencyInjectionConfig.Register);

            //Configure automapper for mapping entities
            AutoMapperConfiguration.Configure();
        }
    }
}